﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace hw23082020
{
    class Program
    {
        static void Main(string[] args)
        {
            MyQueue myQueue = new MyQueue();


            myQueue.PrintList("start list");
            Console.WriteLine("add 2 customer");
            myQueue.Enqueue(new Customer(1, "a1", 1990, "bla", 3, 10));
            myQueue.Enqueue(new Customer(2, "a2", 1995, "bla", 1, 10));
            myQueue.PrintList("After Adding customers");

            Console.WriteLine("clean");
            myQueue.Clear();
            myQueue.PrintList("after clean");

            Console.WriteLine("add new customers");
            myQueue.Enqueue(new Customer(3, "a3", 1920, "bla", 3, 10));
            myQueue.Enqueue(new Customer(4, "a4", 1970, "bla", 15, 10));
            myQueue.Enqueue(new Customer(5, "a5", 1960, "bla", 8, 10));
            myQueue.Enqueue(new Customer(6, "a6", 1930, "bla", 3, 10));
            myQueue.Enqueue(new Customer(7, "a7", 1950, "bla", 7, 10));
            myQueue.Enqueue(new Customer(8, "a8", 1988, "bla", 15, 10));
            myQueue.Enqueue(new Customer(9, "a9", 1989, "bla", 5, 10));
            myQueue.PrintList("After Adding  customers for now");


            Console.WriteLine("Dequeue()");
            Console.WriteLine(myQueue.Dequeue());
            myQueue.PrintList("list after Dequeue");

            Console.WriteLine("replace list");
            myQueue.Init(new List<Customer>()
            {
                new Customer(1, "a3", 1920, "bla", 3, 10),
                new Customer(4, "a4", 1970, "bla", 15, 2),
                new Customer(5, "a5", 1960, "bla", 8, 1),
                new Customer(10, "a6", 1930, "bla", 3, 20),
                new Customer(7, "a7", 1950, "bla", 7, 13),
                new Customer(15, "a8", 1988, "bla", 15, 18)
            }
            );

            myQueue.PrintList("after replace list ");

            Console.WriteLine("myQueue.WhoIsNext()");
            Console.WriteLine($"{myQueue.WhoIsNext()}");


            Console.WriteLine("myQueue.Count");
            Console.WriteLine($"{myQueue.Count}");

            Console.WriteLine("myQueue.SortByProtection()");
            myQueue.SortByProtection();
            myQueue.PrintList("after SortByProtection ");


            Console.WriteLine("myQueue.SortByTotalPurchases()");
            myQueue.SortByTotalPurchases();
            myQueue.PrintList("after SortByTotalPurchases ");



            Console.WriteLine("myQueue.SortByBirthYear()");
            myQueue.SortByBirthYear();
            myQueue.PrintList("after SortByBirthYear ");

            Console.WriteLine("myQueue.DequeueCustomers - defulte case");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(2, 0), "the return list of DequeueCustomers : option 2 - delete 0...x and return 0...x (the removed items) ");
            myQueue.PrintList("after DequeueCustomers");

            Console.WriteLine("myQueue.DequeueCustomers- case number 1");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(3, 0), "the return list of DequeueCustomers : option 2 - delete 0...x and return 0...x (the removed items) ");
            myQueue.PrintList("after DequeueCustomers");


            Console.WriteLine("myQueue.DequeueCustomers- case number 0");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(3, 0), "the return list of DequeueCustomers : option 2 - delete 0...x and return 0...x (the removed items) ");
            myQueue.PrintList("after DequeueCustomers");



            Console.WriteLine("========================replace the list==================================================");
            myQueue.Init(new List<Customer>()
                {
                    new Customer(1, "a3", 1920, "bla", 3, 10),
                    new Customer(4, "a4", 1970, "bla", 15, 10),
                    new Customer(5, "a5", 1960, "bla", 8, 10),
                    new Customer(10, "a6", 1930, "bla", 3, 10),
                    new Customer(7, "a7", 1950, "bla", 7, 10),
                    new Customer(15, "a8", 1988, "bla", 15, 10)
                }
            );


            Console.WriteLine("myQueue.DequeueCustomers - defulte case");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(2, 1), "the return list of DequeueCustomers : option 1 - delete 0...x and return 0...x (from the new list after remove) ");
            myQueue.PrintList("after DequeueCustomers");

            Console.WriteLine("myQueue.DequeueCustomers- case number 1");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(3, 1), "the return list of DequeueCustomers : option 1 - delete 0...x and return 0...x (from the new list after remove) ");
            myQueue.PrintList("after DequeueCustomers");


            Console.WriteLine("myQueue.DequeueCustomers- case number 0");
            myQueue.PrintList("before DequeueCustomers");
            PrintList(myQueue.DequeueCustomers(3, 1), "the return list of DequeueCustomers : option 1 - delete 0...x and return 0...x (from the new list after remove) ");
            myQueue.PrintList("after DequeueCustomers");


            Console.WriteLine("========================replace the list==================================================");
            myQueue.Init(new List<Customer>()
                {
                    new Customer(1, "a3", 1920, "bla", 3, 10),
                    new Customer(4, "a4", 1970, "bla", 15, 13),
                    new Customer(5, "a5", 1960, "bla", 8, 12),
                    new Customer(10, "a6", 1930, "bla", 3, 10),
                    new Customer(7, "a7", 1950, "bla", 7, 10),
                    new Customer(15, "a8", 1988, "bla", 15, 10)
                }
            );

            Console.WriteLine("AniRakSheela test ");
            myQueue.PrintList("print currect order: ");
            Console.WriteLine("add the customer");
            myQueue.AniRakSheela(new Customer(299,"shela",2000,"b",1,0));
            myQueue.PrintList("after AniRakSheela");

            Console.WriteLine("protexia ");
            Console.WriteLine($" pro customer : {myQueue.DequeueProtectzia()}");
            myQueue.PrintList("after prote");

            Console.WriteLine("protexia ");
            Console.WriteLine($" pro customer : {myQueue.DequeueProtectzia()}");
            myQueue.PrintList("after prote2");

            Console.WriteLine("protexia ");
            Console.WriteLine($" pro customer : {myQueue.DequeueProtectzia()}");
            myQueue.PrintList("after prote3");

        }

        public static void PrintList(List <Customer> customers, string headLine = "")
        {
            Console.WriteLine($"---------------------------------");
            Console.WriteLine($"{headLine}");
            Console.WriteLine($"---------------------------------");

            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer);
            }

            Console.WriteLine();
        }

    }
}
