﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;

namespace hw23082020
{
    public class MyQueue
    {
        private const int EmptyList = 0;
        
        private List<Customer> customers= new List<Customer>();

        public void Enqueue(Customer customer)
        {
            customers.Add(customer);
        }

        public Customer Dequeue()
        {
            Customer returnCustomer = null;
            if (customers.Count== EmptyList)
            {
                return null;
            }

            returnCustomer = customers[0];
            customers.RemoveAt(0);
            return returnCustomer;
        }

        public void Init(List<Customer> newList)
        {
            customers = newList;
        }

        public void Clear()
        {
            customers.Clear();
        }

        public Customer WhoIsNext()
        {
            if (customers.Count > EmptyList)
            {
                return customers[0];
            }

            return null;
        }

        public int Count
        {
            get
            {
                return customers.Count;
            }

        }

        public void SortByProtection()
        {
            customers.Sort(new CustomerSortByProtection());
        }

        public void SortByTotalPurchases()
        {
            customers.Sort(new CustomerSortByTotalPurchases());

        }

        public void SortByBirthYear()
        {
            customers.Sort(new CustomerSortByBirthYear());

        }


        public List<Customer> DequeueCustomers(int number, int option = 1)
        {
            //if the list is empty return empty list 
            if (customers.Count == EmptyList)
            {
                return new List<Customer>();
            }

            if (option == 1)
            {
                return Option1DequeueCustomers(number);
            }
            else
            {
                return Option2DequeueCustomers(number);
            }
        }


        /// <summary>
        /// Example of the function activity:
        ///The list has 6 people, we got the number 2
        ///If we look at the list we have the locations:
        ///0,1,2,3,4,5,6
        ///We will delete locations 0,1
        ///And we will return a new list with 3,4 (the position before the deletion we made)
        /// </summary>
        /// <param name="number">Indicates a number of people</param>
        /// <returns></returns>
        private List<Customer> Option1DequeueCustomers(int number)
        {
            //x = 0: My queue is shorter than the number the function received.
            //x = 1 The queue is equal to or greater than the number we received, but the list we return will be smaller than the number we received.
            //Default mode: The x is greater than or equal to x * 2 so we can delete the number we received and also return the next five customers.
            int x = customers.Count / number;
            switch (x)
            {
                case 0:
                    customers.RemoveRange(0, customers.Count);
                    return new List<Customer>();
                //break; // it is not used I know 

                case 1:
                    customers.RemoveRange(0, number);
                    return customers.GetRange(0, customers.Count);
                    //break; // it is not used I know 
                default:
                    customers.RemoveRange(0, number);
                    return customers.GetRange(0, number);
                    //break; // it is not used I know 
            }
        }

        /// <summary>
        /// Example of the function activity:
        /// The list has 6 people, we got the number 2
        ///If we look at the list we have the locations:
        ///0,1,2,3,4,5,6
        ///We will delete the positions 0,1
        ///And return a new list with 0,1 (the position before the deletion we made)
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private List<Customer> Option2DequeueCustomers(int number)
        {
            List<Customer> returnCustomerslList = null;

            //x = 0: My queue is shorter than the number the function received.
            //x = 1 The queue is equal to or greater than the number we received, but the list we return will be smaller than the number we received.
            //Default mode: The x is greater than or equal to x * 2 so we can delete the number we received and also return the next five customers.
            int x = customers.Count / number;
            switch (x)
            {
                case 0:
                    returnCustomerslList = customers.GetRange(0, customers.Count);
                    customers.RemoveRange(0, customers.Count);
                    return returnCustomerslList;
                  //  break; // it is not used I know 

                case 1:
                    returnCustomerslList = customers.GetRange(0, number);
                    customers.RemoveRange(0, number);
                    return returnCustomerslList;
                    //break; // it is not used I know 

                default:
                    returnCustomerslList = customers.GetRange(0, number);
                    customers.RemoveRange(0, number);
                    return returnCustomerslList;
                    //break; // it is not used I know 
            }
        }


        public void AniRakSheela(Customer customer)
        {
            customers.Insert(0,customer);
        }

        public Customer DequeueProtectzia()
        {
            if (customers.Count== EmptyList)
            {
                return null;
            }

            Customer maxProtectziaCustomer = null;
            int indexCustomerWithMaxProtectzia = 0; // the first Customer before check.

            for (int i = 1; i < customers.Count; i++) // i = 1 because we have already taken the value of i = 0
            {
                if (customers[i].Protection > customers[indexCustomerWithMaxProtectzia].Protection)
                {
                    indexCustomerWithMaxProtectzia = i;
                }
            }

            maxProtectziaCustomer = customers[indexCustomerWithMaxProtectzia];
            customers.RemoveAt(indexCustomerWithMaxProtectzia);
            return maxProtectziaCustomer;
        }


        public  void PrintList(string headLine = "")
        {
            Console.WriteLine($"---------------------------------");
            Console.WriteLine($"{headLine}");
            Console.WriteLine($"---------------------------------");

            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer);
            }

            Console.WriteLine();
        }

    }



}