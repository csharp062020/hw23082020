﻿using System.Collections.Generic;

namespace hw23082020
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public string Address { get; set; }
        public int Protection { get; set; }
        public int TotalPurchases { get; set; }

        public Customer(int id, string name, int birthYear, string address, int protection, int totalPurchases)
        {
            ID = id;
            Name = name;
            BirthYear = birthYear;
            Address = address;
            Protection = protection;
            TotalPurchases = totalPurchases;
        }

        public override string ToString()
        {
            return $"{nameof(ID)}: {ID}, {nameof(Name)}: {Name}, {nameof(BirthYear)}: {BirthYear}, {nameof(Address)}: {Address}, {nameof(Protection)}: {Protection}, {nameof(TotalPurchases)}: {TotalPurchases}";
        }

    }


    class CustomerSortByProtection : IComparer<Customer>
    {

        public int Compare(Customer x, Customer y)
        {
            return x.Protection - y.Protection;
        }
    }

    class CustomerSortByTotalPurchases : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.TotalPurchases - y.TotalPurchases;
        }
    }

    class CustomerSortByBirthYear : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.BirthYear - y.BirthYear;
        }
    }
}